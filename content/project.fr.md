---
title: "Le projet"
description: "Description du projet et l'équipe derrière le DigiPartIndex"
bg_image: "images/background/unsplash-W8KTS-mhFUE-2048w.jpg"
layout: "project"
draft: false
menu:
  main:
    name: "Projet"
    weight: 2
  footer:
    name: "Projet"
    weight: 2

########################### project #############################
about:
  enable: true
  title: "DigiPartIndex"
  column_left: "Nous présentons un index faisant le point sur la participation politique digitale en Suisse et fournissant un tableau comparatif entre cantons. Un tel suivi n'existe pas encore en Suisse, et peut servir d'incitation à creuser la recherche sur la participation digitale dans le pays.


  Le concept de \"participation politique digitale\" doit être saisi dans une gamme de valeurs normalisées grâce à un index. Nous nous concentrerons sur les outils digitaux qui permettent la participation politique effective. Nous commencerons par couvrir tous les cantons suisses, mais nous pourrions étendre l'index à certaines villes et municipalités, jusqu'à peut-être évaluer le niveau fédéral.


  Qu'entendons-nous par participation politique digitale au sens strict ? L'ONU a défini la participation digitale – soit un synonyme de la participation politique digitale – comme \"un processus qui inclut des citoyens dans la conception, la prise de décision et la mise en œuvre de politiques à travers des technologies d'information et de communication, dans le but de rendre ce processus participatif, inclusif et délibératif.\"


  Au sein de notre projet, nous veillons à inclure le plus largement possible tous segments de population."
  column_right: "La participation digitale n'est pas liée uniquement à la citoyenneté suisse. De plus, les offres de participation politique digitale ne viennent pas seulement d'agences gouvernementales (logique top-down), mais peuvent aussi émaner d'organisations de la société civile (logique bottom-up).


  Le concept de \"participation politique digitale\" inclut donc les opportunités de faire entendre sa voix durant les différentes phases d'un cycle décisionnel politique (définition du problème, établissement d'un agenda, formulation et prise d'une décision, mise en œuvre, évaluation), ceci à l'aide de technologies d'information et communication. La participation digitale influence également des décisions juridiques ou leur préparation par les institutions étatiques. Ces outils digitaux et applications peuvent, mais ne doivent pas, être fournis par l'Etat.


  En clair, la participation politique digitale implique la facilitation grâce à Internet des échanges entre la population, la société civile et les agences gouvernementales à différents stades du processus politique."

########################## institutions ############################
institutions:
  enable: true
  item:
    - image: "images/logos/mercator_white.svg"
      url: "https://www.stiftung-mercator.ch/"
      color: "primary"
      content: ""

    - image: "images/logos/zda_white.svg"
      url: "https://zdaarau.ch/"
      color: "primary-dark"
      content: ""

    - image: "images/logos/procivis_white.svg"
      url: "https://www.procivis.ch/think-thank/"
      color: "primary-darker"
      content: ""

########################## team ############################
team:
  enable: true
  title: "Equipe et partenaires"
  intro: "DigiPartIndex est fondé par le programme [\"Digitalisation et société\" de la Fondation Mercator Suisse](https://www.stiftung-mercator.ch/stiftung/digitalisierung-gesellschaft). L'équipe du projet s'est constituée en partenariat entre le [Centre pour la Démocratie Aarau (ZDA)](https://zdaarau.ch/), affilié à l'Université de Zürich, et le [think tank Procivis S.A.](https://www.procivis.ch/think-thank/)"
  item:
    - image: "images/people/uwe.jpg"
      name: "Uwe Serdült"
      content: "**Uwe Serdült** (chef de projet) est un associé de recherche au Centre pour la Démocratie Aarau (ZDA) de l'Université de Zürich et Professeur au \"e-Society Laboratory\" à l'Université de Ritsumeikan, au Japon. De 2012 à 2018, il a mené le projet d'envergure basé à la ZDA \"E-Democracy\", ainsi qu'un projet FNS interdisciplinaire sur \"L'émotivité et la polarisation dans les nouveaux médias sociaux\" en collaboration avec le Prof. F. Schweitzer, de l'Ecole polytechnique fédérale de Zürich - ETHZ (2013-2016). Ses activités en sciences et médias dans le domaine de la démocratie digitale sont à découvrir sur [son site web](https://uweserdult.wordpress.com/)."

    - image: "images/people/costa.jpg"
      name: "Costa Vayenas"
      content: "**Costa Vayenas** (chef de projet) est l'auteur de l'ouvrage [\"Democracy in the Digital Age\"](https://books.google.ch/books/about/Democracy_in_the_Digital_Age.html?id=IpCfDwAAQBAJ). Il préside le think tank de Procivis, qui étudie l'impact des technologies numériques sur la démocratie. En plus d'un large éventail d'activités d'enseignement en Suisse ainsi que d'invitations à fournir des contributions politiques (notamment au Parlement européen dans le domaine de la cyberadministration), il enseigne actuellement à l'Ecole polytechnique fédérale de Zürich, dans le nouveau programme postdoctoral [\"Technology and Public Policy Programme\"](https://tpp.ethz.ch/)."

    - image: "images/people/gabriel.jpg"
      name: "Gabriel Hofmann"
      content: "**Gabriel Hofmann** est assistant de recherche au Centre pour la Démocratie Aarau (ZDA) de l'Université de Zürich. Etudiant en sciences politiques, il se concentre sur la politique suisse à l'Institut de Sciences politiques de Zürich, où il a aussi oeuvré comme assistant de recherche, cette fois à la Chaire d'analyse et d'évaluation des politiques. Durant ses études, il s'est beaucoup intéressé aux compétences civiques et aux processus de prise de décision."

    - image: "images/people/marine.jpg"
      name: "Marine Benli-Trichet"
      content: "**Marine Benli-Trichet** est chercheuse au Centre pour la Démocratie Aarau (ZDA). Depuis 2018, elle est doctorante à l'Institut de Science Politique de l'Université de Zurich. Elle travaille principalement sur le thème de la digitalisation et la politique locale. Dans sa thèse, elle étudie l'impact des technologies civiques sur la démocratie. Par ailleurs, elle a été impliquée dans l’édition 2021 du projet [\"Baromètre des civic tech\"](https://www.epfl.ch/labs/lasur/fr/barometre-des-civic-tech-2021/) en collaboration avec le Laboratoire de Sociologie Urbaine (LaSUR) de l'Ecole polytechnique fédérale de Lausanne (EPFL).  "


########################## partners ############################

  partner:
    enable: true
    title: "Partenaires"
    intro: "Outre le ZDA et le thinktank de Procivis, d'autres institutions sont associées au projet. Depuis l'été 2022, il existe notamment une collaboration avec l'[Università della Svizzera Italiana (USI)](https://www.usi.ch/en)."
    item:
      - image: "images/people/jeanpatrick.jpg"
        name: "Jean-Patrick Villeneuve"
        content: "**Jean-Patrick Villeneuve** Jean-Patrick Villeneuve est professeur associé à l'Università della Svizzera italiana (USI) de Lugano. Il est directeur de l'Institut de communication et de politique publique et du GRIP (Groupe de recherche sur l'intégrité publique). Il est membre fondateur de la Global Conference on Transparency Research et, jusqu'à récemment, membre du groupe d'experts indépendants de l'Open Government Partnership.


        Ses recherches portent sur les questions de participation civique, de transparence, de lutte contre la corruption et de responsabilité. Il s'intéresse particulièrement aux limites, aux difficultés et aux impacts de la mise en œuvre de ces initiatives de gouvernance. Ses recherches ont été financées par le Fonds national suisse de la recherche scientifique (SNSF), le Swiss Network for International Studies (SNIS), Swissuniversities et les sciences sociales, le Conseil de recherches en sciences humaines du Canada (CRSH) et d’autres organismes. Professeur Villeneuve a travaillé aux Nations Unies et dans différentes organisations publiques."

      - image: "images/people/anna.jpg"
        name: "Anna Picco-Schwendener"
        content: "**Anna Picco-Schwendener**, PhD, est chercheure postdoctorale à la Faculté de Communication, Culture et Société de l'Università della Svizzera italiana (USI) où elle enseigne le E-Government et le Online Communication Design. Elle travaille également en tant que collaboratrice scientifique au eLearning Lab de l'USI où elle est responsable du [Centre de compétence en droit numérique](http://www.ccdigitallaw.ch) et représente l'USI au sein de l'unité opérationnelle du [Lugano Living Lab](https://luganolivinglab.ch). Sa thèse de doctorat portait sur les \"Social Dimensions of Public Large-Scale Wi-Fi Networks: The Cases of a Municipal and a Community Wireless Network\"."

######################### former teammembers #######################

  former_team:
    enable: true
    title: "Anciens membres de l'équipe"
    item:
      - image: "images/people/herveline.jpg"
        name: "Herveline Du Clary"
        content: "**Herveline Du Clary**, grâce à ses connaissances linguistiques, culturelles et en matière de communication, s'est occupée de la traduction française du site web et du [rapport 2021](/fr/blog/digipartindex_2021/) jusqu'à la fin de l'année 2021."

      - image: "images/people/leonardo.jpg"
        name: "Leonardo Colasante"
        content: "**Leonardo Colasante** s'est occupé de la traduction italienne du [rapport 2021](/fr/blog/digipartindex_2021/) et de la collecte des données pour le canton du Tessin de mi-2022 à début 2023, parallèlement à ses études de master en Public Management and Policy à l'Università della Svizzera italiana (USI) à Lugano."

############################# contact #################################
contact:
  enable: false
  # content comes from "_index.md"
---
