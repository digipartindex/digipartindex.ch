---
title: "Introducing the DigiPartIndex: Mapping and explaining digital political participation on the subnational level in Switzerland"
date: 2022-11-18T21:00:00+02:00
author: [ "Uwe Serdült", "Gabriel Hofmann", "Costa Vayenas"]
image: "images/blog/ice_gov_2022.png"
bg_image: ""
categories: ["articles"]
tags: ["Civic Tech", "Democracy", "Digital Participation", "E-Governance", "E-Voting", "E-Consultation", "E-Deliberation", "E-Collecting", "E-ID", "Federalism"]
description: "Introducing the DigiPartIndex"
draft: true
type: "post"
---

Digital political participation increasingly complements analogue forms of political participation. Elements of the political process such as dialogue, consultation, participation as well as voting have received a further digital boost in the COVID-19 pandemic. Because they reflect the new digital experiences of ever broader sections of the population, using digital means to participate in the political process will play an increasingly important role in the future. The DigiPartIndex (DPI) measures three dimensions of digital political participation for all cantons in Switzerland. The first dimension reflects how political decision-making in democracies is preceded by an opinion-formation phase. It covers tools for e-deliberation, digital political education and e-transparency. The second dimension, co-creation, maps the exchange between government agencies and civil society. The two components, e-consultation and e-demand, are surveyed for this purpose. Thirdly, in addition to public debate and an exchange between the state and society, digital tools can also be used to enable the act of voting. To this end, the foundations must be laid in the form of electronic identification, i.e. an e-ID, so that it can then be used for e-voting and  e-collecting, among other things. The values for the DigiPartIndex Switzerland range from 0 to 100 points. Results show that the differences between the cantons are considerable,  anging from a minimum of 6 to a maximum of 55 points. The mean value is 31 points. The ranking tends to be led by cantons with greater financial resources. However, even the cantons at the top range of the index still have room for considerable improvement in all dimensions.

L'intero articolo è disponibile [qui](https://dl.acm.org/doi/10.1145/3560107.3560145).
