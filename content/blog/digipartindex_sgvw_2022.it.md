---
title: "Möglichkeiten digitaler politischer Partizipation in den Kantonen der Schweiz – Wie erklären sich die grossen Unterschiede?"
date: 2022-11-22T21:00:00+02:00
author: [ "Uwe Serdült", "Gabriel Hofmann"]
image: "images/blog/sgvw_paper.png"
bg_image: ""
categories: ["articles"]
tags: ["Civic Tech", "Democracy", "Digital Governance", "E-Democracy", "E-Participation", "Federalism", "E-Voting"]
description: "Möglichkeiten digitaler politischer Partizipation"
draft: true
type: "post"
---

Digitale politische Partizipation ergänzt zunehmend analoge Formen politischer Beteiligung. Elemente des politischen Prozesses wie Dialog, Konsultation, Beteiligung sowie Abstimmen und Wählen im digitalen Raum haben gerade in Zeiten von COVID-19 einen Schub erhalten. Weil sie den neuen digitalen Lebenswelten immer breiterer Bevölkerungskreise entsprechen, werden sie auch in Zukunft eine wichtige Rolle spielen. Wie stark sich dieses neue Phänomen in der Schweiz bisher niedergeschlagen hat, war bisher jedoch schwierig fassbar. Der neu geschaffene DigiPartIndex misst deshalb die Möglichkeiten digitaler politischer Partizipation auf kantonaler Ebene in der Schweiz auf einer Skala von 0-100. Ein solches Monitoring gibt es für die Schweiz noch nicht, ist aber nötig, um gegenwärtig ablaufende Prozesse der Digitalisierung besser zu verstehen. Der Index hat aber auch einen praktischen Wert, indem er für kantonale Behörden sowie die interessierte Öffentlichkeit Orientierungswissen bereitstellt. Er umfasst die drei Dimensionen Meinungsbildung, Mitwirkung und Entscheidung mit insgesamt sieben Indikatoren. Die Datenerhebung erfolgte im Sommer/Herbst 2021. Als Resultat zeigen sich grosse Unterschiede. Erzielt wurden minimal 6 bis maximal 55 Punkte. Auch die an der Spitze liegenden Kantone können sich noch in allen Bereichen verbessern. Ziel dieses Artikels ist es, erste mögliche Erklärungen der vorgefundenen Unterschiede zu testen. Für die empirische Prüfung werden als erklärende Faktoren Bevölkerungsstruktur, Wirtschaftskraft sowie Offenheit des politischen Systems herangezogen. Finanzstarke Kantone führen die Rangliste an. Kantone mit einem höheren Anteil von über 65-Jährigen bieten weniger digitale Partizipationsmöglichkeiten an. Zwischen der Offenheit des politischen Systems in einem Kanton und der Verbreitung digitaler Partizipationsmöglichkeiten besteht kein eindeutiger Zusammenhang. Neben der Interpretation der Resultate werden zum Schluss auch Grenzen der vorliegenden Studie diskutiert sowie mögliche weiterführende Fragestellungen entwickelt.

L'intero articolo è disponibile [qui](https://doi.org/10.5334/ssas.172).
