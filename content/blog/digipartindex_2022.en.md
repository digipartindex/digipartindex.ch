---
title: "The midfield is catching up"
date: 2022-10-10T07:00:00+02:00
author: ["Gabriel Hofmann", "Uwe Serdült", "Costa Vayenas", "Marine Benli-Trichet", "Jean-Patrick Villeneuve", "Anna Picco-Schwendener", "Leonardo Colosante"]
image: "images/blog/dpi_ordered_21_22.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "DigiPartindex Report 2022"
draft: false
type: "post"
---

The DigiPart Index (DPI) records the extent to which it is possible to participate digitally in political processes in Switzerland's cantons on a scale of 0 to 100. In the second year of this survey, the average score across all cantons is still relatively low, but rose from 31 to 33 points. The ranking of the cantons shows that digital participation in political processes can be improved in all cantons. At the same time, the cantons in the lower midfield are catching up and competing with the cantons at the top. This is due, on the one hand, to small, gradual improvements in all areas covered by the index and, on the other hand, to a certain instability regarding the supply and use of tools in the "participation" dimension.

Digital political participation is recorded using an index in a standardized value range. The values for the DigiPart Index Switzerland range from 0 to 100 points and include the dimensions "opinion formation," "participation," and "decision-making”. Also in 2022, the differences between the cantons are also considerable. A minimum of 5 to a maximum of 56 points were scored. The mean value is 33 points. Financially stronger cantons tend to lead the ranking. Cantons with an older population structure tend to offer fewer digital participation opportunities. However, even the cantons at the top still have room to improve in all areas.

Compared to the previous year, there have been fluctuations, particularly in the "participation" dimension. This instability can be observed both in "eConsultation" and "eObjections" and is primarily due to discontinued pilot tests or lower use of the corresponding platforms by civil society. In the dimension of "opinion formation", minor improvements were observed in some cantons for the areas of "eDeliberation", "ePolitical education" and "eTransparency". Finally, in the dimension of "Decision-making" -- especially in the area of "eID" -- minor improvements were also recorded for a few cantons. Nothing has changed in the area of "eVoting".

The index is updated annually. The full report (in German) is available below and the data set is available [**hier**](/en/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2022_de" alt="Bericht DigiPartIndex 2022" >}}](/docs/DigiPartIndex_2022_de.pdf)
