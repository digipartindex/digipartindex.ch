---
title: "Digital Political Participation in Seven Countries: Case Studies on Digital Political Participation in Austria, France, Canada, Germany, Italy, Japan and Switzerland"
date: 2023-03-15T21:00:00+02:00
author: [ "Uwe Serdült", "Gabriel Hofmann", "Marine Benli-Trichet", "Costa Vayenas", "Jean-Patrick Villeneuve", "Anna Picco-Schwendener", "Leonardo Colosante"]
image: "images/blog/fallstudie_icegov_2023.png"
bg_image: ""
categories: ["case_studies"]
tags: ["Civic Tech", "Democracy"]
description: "Digital Political Participation in Seven Countries"
draft: true
type: "post"
---

This short paper presents seven short case studies on digital political participation in Switzerland, Germany, Austria, Italy, France, Canada and Japan. Digital political participation is measured with the DigiPartIndex (DPI). In the analysed countries we find different levels of digital political participation with DPI-scores ranging between 33 and 65 points.

L'étude de case entière est disponible [ici](/docs/case_studies_icegov_2023.pdf).
