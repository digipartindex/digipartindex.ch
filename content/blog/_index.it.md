---
title: "Rapporti"
description: "Rapporti annuali sul DigiPartIndex"
draft: false
bg_image: "images/background/unsplash-Oaqk7qqNh_c-edited-2048w"
menu:
  main:
    name: "Rapporti"
    identifier: "blog"
    weight: 3
  footer:
    name: "Rapporti"
    identifier: "blog"
    weight: 3
---
