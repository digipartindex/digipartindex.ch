---
title: "Participation numérique – la collaboration entre les cantons est nécessaire"
date: 2024-01-15T14:00:00+01:00
author: [
  "Uwe Serdült",
  "Gabriel Hofmann",
  "Marine Benli-Trichet",
  "Stefan Kalberer",
  "Jonas Wüthrich",
  "Costa Vayenas",
  "Jean-Patrick Villeneuve",
  "Anna Picco-Schwendener"
]
image: "images/blog/dpi_map_2023.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "Rapport DigiPartindex 2023"
draft: false
type: "post"
---

Le DigiPart-Index mesure, sur une échelle de 0 à 100, dans quelle mesure il est possible de participer numériquement aux processus politiques dans les cantons suisses. Pour la troisième année consécutive, la valeur moyenne de tous les cantons reste relativement basse. Une fois de plus, le canton de tête et celui en bas du classement se distinguent par un facteur dix. Alors que dans les cantons se trouvant en bas du classement, peu d'actions ont été entreprises en matière de participation numérique au cours des trois dernières années, les cantons en tête de classement ont enregistré une nouvelle augmentation de leurs scores cette année. Cela s'explique notamment par la réintroduction du vote électronique.

En Suisse, les formes numériques de participation politique, telles que le vote électronique ou les sondages en ligne, complètent de plus en plus les méthodes traditionnelles, comme le bulletin de vote et les formulaires imprimés. Cela correspond à l'environnement numérique d'une partie de plus en plus large de la population. C'est pourquoi une équipe interdisciplinaire de chercheurs a établi pour la Suisse un indice de participation politique numérique. Ce dernier permet de comparer de manière systématique les possibilités de participation numérique à travers les cantons.  L'indice DigiPart Suisse est mesuré sur une échelle de valeurs standardisées allant de 0 à 100 points, englobant les dimensions de "formation d'opinion", "participation" et "prise de décision".

### Des différences croissantes entre les cantons

Pour la troisième année de l'enquête DigiPart, les différences entre les cantons restent considérables et ont même connu une légère augmentation . Les points obtenus vont d'un minimum de 6 à un maximum de 58. La valeur moyenne est de 33 points. Les petits cantons ruraux ont tendance à se trouver en bas du classement et à n'offrir que des instruments très limités en matière de participation politique numérique. Cependant, même parmi les cantons en tête de classement, il reste une marge de progression, notamment dans le domaine des e-requêtes, où seules des plateformes privées comme "petitio" ou "weCollect" sont actuellement disponibles. 

Tandis que les cantons de tête se situent dans la zone moyenne élevée de l'indice DigiPart, les cantons en bas de classement n'ont connu que peu d’évolutions au cours des trois dernières années. Au vu de ces grandes disparités, il convient de veiller à ce que les plus petits cantons et ceux aux ressources plus faibles ne soient pas distancés. Par exemple, en renforçant la collaboration (technique) entre les cantons, cela permettrait aux plus petits de profiter du savoir-faire des plus grands. Des solutions techniques existent déjà dans les différents cantons, à l'exception du domaine des e-requête. Grâce à l'apprentissage mutuel, ces solutions pourraient également être utilisées dans d'autres cantons. Dans une Suisse plurilingue et de petite taille, une telle collaboration s’avérerait particulièrement pertinente pour l'avenir, compte tenu de la rapidité avec laquelle les changements se produisent. C'est ce que montre le développement rapide de nouveaux logiciels tels que ChatGPT, susceptibles d’influencer le domaine de la participation politique sous forme numérique.

L'indice est mis à jour chaque année. Le rapport (en allemand) peut être téléchargé ci-après, quant à l'ensemble des données, elles sont disponibles [**ici**](/fr/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2023_de" alt="Bericht DigiPartIndex 2023" >}}](/docs/DigiPartIndex_2023_de.pdf)
