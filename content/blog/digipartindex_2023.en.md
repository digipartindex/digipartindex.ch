---
title: "Digital participation – cooperation between the cantons is needed"
date: 2024-01-15T14:00:00+01:00
author: [
  "Uwe Serdült",
  "Gabriel Hofmann",
  "Marine Benli-Trichet",
  "Stefan Kalberer",
  "Jonas Wüthrich",
  "Costa Vayenas",
  "Jean-Patrick Villeneuve",
  "Anna Picco-Schwendener"
]
image: "images/blog/dpi_map_2023.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "DigiPartindex Report 2023"
draft: false
type: "post"
---

On a scale of 0 to 100, the DigiPart-Index measures the extent to which it is possible to participate digitally in political processes in Switzerland's cantons. In the third survey year, the average score across all cantons remains relatively low. For the third time in a row, the top and bottom cantons differ by a factor of ten. While little has changed in terms of digital participation in the cantons in the bottom group over the past three years, there has been a further increase in the top group this year. This is partly due to the reintroduction of e-voting.

Digital forms of political participation such as e-voting or online surveys are increasingly supplementing conventional forms such as ballot papers and printed forms throughout Switzerland. This corresponds to the digital experience of ever broader sections of the population. Accordingly, an interdisciplinary team of researchers is therefore compiling an index for Switzerland that records digital political participation and provides a comparable picture of digital participation opportunities in the cantons. The index is recorded in a standardized value range. The values for the DigiPart Index Switzerland range from 0 to 100 points and cover the dimensions of "opinion-forming", "participation" and "decision-making".

### Increasing differences between the cantons

In the third year of the DigiPart Index survey, the differences between the cantons are still considerable and have even increased slightly. A minimum of 6 to a maximum of 58 points were achieved. The average value is 33 points. Smaller rural cantons tend to be in the bottom group and only offer very limited instruments for digital political participation. But even in the top group, there is room for further development, particularly in the category of e-demand, where there are currently only private platforms such as "petitio" or "weCollect". 

While the top cantons are in the high middle range of the DigiPart Index, there has been little change in the cantons in the bottom group over the past three years. In view of such large differences, caution is required to ensure that the smaller and less resourced cantons are not left behind. For example, by strengthening (technical) cooperation between the cantons so that smaller cantons can benefit from the know-how of the larger cantons. Apart from in the area of e-demand, technical solutions are already available in the individual cantons. Through mutual learning, these could also be used in other cantons. In small-scale and multilingual Switzerland, such cooperation is particularly relevant with a view to the future. This is demonstrated by the rapid development of new software such as ChatGPT. Such developments will influence the field of political participation in digital form.

The index is updated annually. The full report (in German) is available below and the data set is available [**hier**](/en/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2023_de" alt="Bericht DigiPartIndex 2023" >}}](/docs/DigiPartIndex_2023_de.pdf)
