---
title: "Das Mittelfeld hat aufgeholt"
date: 2022-10-10T07:00:00+02:00
author: ["Gabriel Hofmann", "Uwe Serdült", "Costa Vayenas", "Marine Benli-Trichet", "Jean-Patrick Villeneuve", "Anna Picco-Schwendener", "Leonardo Colosante"]
image: "images/blog/dpi_ordered_21_22.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy", "Participation", "Federalism", "Switzerland"]
description: "DigiPartindex-Bericht 2022"
draft: false
type: "post"
---

Der DigiPartIndex (DPI) erfasst auf einer Skala von 0 bis 100, inwiefern es in den Kantonen der Schweiz möglich ist, sich digital an politischen Prozessen zu beteiligen. Im zweiten Jahr seiner Erhebung ist der Durchschnittswert über alle Kantone hinweg immer noch relativ tief, stieg aber von 31 auf 33 Punkte an. Im Ranking der Kantone wird ersichtlich, dass bei allen Kantonen die digitale Partizipation in politischen Prozessen verbessert werden kann. Gleichzeitig holen die Kantone des unteren Mittelfeldes auf und konkurrenzieren die Kantone an der Spitze. Dies liegt einerseits an kleinen, schrittweisen Verbesserungen in allen vom Index erfassten Bereichen und andererseits an einer gewissen Instabilität betreffend Angebot und Verwendung von Tools in der Dimension «Mitwirkung».

Digitale politische Partizipation ergänzt zunehmend analoge Formen politischer Beteiligung. Elemente des politischen Prozesses wie Dialog, Konsultation, Beteiligung sowie Abstimmen und Wählen im digitalen Raum werden zunehmend wichtiger. Denn sie entsprechen den neuen digitalen Lebenswelten und -erfahrungen immer breiterer Bevölkerungskreise. Das Zentrum für Demokratie Aarau (ZDA), der Procivis Think Tank und das Institut für Kommunikation und öffentliche Ordnung und sowie das Institut für digitale Technologien der Kommunikation der Università della Svizzera italiana erheben deshalb für die Schweiz einen Index, der digitale politische Partizipation erfasst und Kantone vergleichbar abbildet.

Digitale politische Partizipation wird mit Hilfe eines Index in einem standardisierten Wertebereich erfasst. Die Werte für den DigiPartIndex Schweiz reichen von 0 bis 100 Punkten und umfassen die Dimensionen «Meinungsbildung», «Mitwirkung» und «Entscheiden». Die Unterschiede zwischen den Kantonen sind auch im Jahr 2022 beträchtlich. Erzielt wurden minimal 5 bis maximal 56 Punkte. Der Mittelwert beträgt 33 Punkte. Tendenziell führen finanzstärkere Kantone das Ranking an. Kantone mit einer älteren Bevölkerungsstruktur bieten eher weniger digitale Partizipationsmöglichkeiten an. Jedoch auch die an der Spitze liegenden Kantone können sich in allen Bereichen verbessern.

Im Vergleich zum Vorjahr haben sich vor allem in der Dimension «Mitwirkung» Schwankungen ergeben. Diese Instabilität ist sowohl im Bereich der «eVernehmlassung» als auch im Bereich der «eAnliegen» zu beobachten und liegt primär an eingestellten Pilotversuchen oder tieferer Nutzung der entsprechenden Plattformen durch die Zivilgesellschaft. In der Dimension «Meinungsbildung» waren kleinere Verbesserungen in einigen Kantonen für die Bereiche der «eDeliberation», der «ePolitischen Bildung» und der «eTransparenz» zu beobachten. Schliesslich sind auch in der Dimension «Entscheiden»  - vor allem im Bereich der «eID» - kleinere Verbesserungen für einige wenige Kantone zu verzeichnen. Im Bereich des «eVoting» hat sich nichts verändert.

Der Index wird jährlich aufdatiert. Der volle Bericht steht nachfolgend zum Download bereit, der Datensatz ist [**hier**](/de/data) abrufbar.

[{{< picture path="images/thumbnails/DigiPartIndex_2022_de" alt="Bericht DigiPartIndex 2022" >}}](/docs/DigiPartIndex_2022_de.pdf)
