---
title: "Différences considérables entre les cantons"
date: 2021-10-25T21:00:00+02:00
author: [ "Uwe Serdült", "Costa Vayenas", "Gabriel Hofmann", "Herveline Du Clary" ]
image: "images/blog/digipartindex_2021.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy"]
description: "Rapport DigiPartindex 2021"
draft: false
type: "post"
---

La première édition de l'**Index de participation politique digitale en Suisse (DigiPartIndex)** mesure, sur une échelle de 0 à 100, les possibilités de participer numériquement au processus politique dans chaque canton suisse. Cette participation politique numérique est déterminée par un total de sept indicateurs, pour trois dimensions que sont la formation de l'opinion, la participation et la prise de décision. Le classement des cantons est sans appel: il y a encore une grande marge de progression. Le canton de Genève a obtenu le meilleur score avec 55 points. L'indice a été lancé par le Centre d'études pour la Démocratie Aarau (ZDA) et le think tank Procivis, soutenus par la Fondation Mercator Suisse.

La participation politique analogique est de plus en plus complétée par des formes numériques. Les éléments du processus politique tels que le dialogue, la consultation, la participation ainsi que le vote et l'élection dans l'espace numérique ont reçu un nouvel élan, en particulier en temps de COVID-19. Ces éléments joueront un rôle de plus en plus important à l'avenir, car ils correspondent aux nouveaux univers de vie numériques et aux expériences de segments grandissants de la population. Le Centre d'études pour la Démocratie Aarau et le think tank Procivis présentent donc un indice suisse qui enregistre la participation politique numérique, et cartographie les cantons de manière comparée, un système de suivi inédit pour la Suisse.

La participation politique digitale est enregistrée à l'aide d'un index dans une fourchette de valeurs standardisées. Les valeurs pour le DigiPartIndex Switzerland vont de 0 à 100 points. Les différences entre les cantons sont considérables. De 6 à 55 points ont été atteints et la valeur moyenne est de 31 points sur 100. Le classement est plutôt mené par les cantons à grande population et aux finances solides. Mais même les cantons les plus performants peuvent encore s'améliorer sensiblement dans tous les domaines.

L'indice mesure trois dimensions de la participation politique numérique. La première dimension montre que les décisions politiques dans les démocraties sont précédées d'une phase de formation de l'opinion. Cette première dimension couvre les outils de délibération en ligne, d'éducation politique numérique et de transparence en ligne. La deuxième dimension, celle de la participation, représente l'échange institutionnalisé entre les agences gouvernementales et la société civile. Les deux composantes e-consultation et e-demandes y sont étudiées. Troisièmement, outre le débat public et l'échange entre l'État et la société, il est possible d'utiliser des outils qui permettent de voter et d'élire numériquement. À cette fin, il faut poser les bases d'une identification électronique, ou e-ID, afin qu'elle puisse être utilisée, entre autres, pour le vote et la collecte électroniques. À noter que des points de bonus ou de pénalité peuvent être attribués pour des critères supplémentaires tels que l'utilisation, l'inclusion, l'ergonomie et la variété.

L'indice sera mis à jour chaque année. Le rapport complet peut être téléchargé ci-dessous, l'ensemble des données est disponible [**ici**](/fr/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2021_fr" alt="Rapport DigiPartIndex 2021" >}}](/docs/DigiPartIndex_2021_fr.pdf)
