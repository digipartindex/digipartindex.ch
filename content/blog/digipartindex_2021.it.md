---
title: "Notevoli differenze tra i cantoni"
date: 2021-10-25T21:00:00+02:00
author: [ "Uwe Serdült", "Costa Vayenas", "Gabriel Hofmann", "Herveline Du Clary" ]
image: "images/blog/digipartindex_2021.png"
bg_image: ""
categories: ["reports"]
tags: ["Civic Tech", "Democracy"]
description: "Rapporto DigiPartindex 2021"
draft: false
type: "post"
---

La prima edizione dell'**Indice di partecipazione politica digitale in Svizzera (DigiPartIndex)** registra il grado in cui è possibile partecipare digitalmente al processo politico nei cantoni svizzeri su una scala da 0 a 100. La partecipazione politica digitale è misurata con l'aiuto di un totale di sette indicatori per le tre dimensioni: formazione delle opinioni, partecipazione e processo decisionale. La classifica dei cantoni mostra che c'è ancora molto margine di miglioramento. Il punteggio più alto è raggiunto dal cantone di Ginevra con 55 punti. L'indice è stato indetto dal Centro per gli studi sulla democrazia di Aarau (ZDA) e dal think tank Procivis, sostenuto dalla Fondazione Mercator Svizzera.

La partecipazione politica digitale completa sempre più le forme analogiche di partecipazione politica. Gli elementi del processo politico come il dialogo, la consultazione, la partecipazione, così come il voto e l'elezione in sistemi digitali, hanno ricevuto un nuovo impulso soprattutto durante il COVID-19. Poiché essi corrispondono ai nuovi mondi di vita digitale e alle esperienze di fasce sempre più ampie della popolazione, avranno un ruolo sempre più importante in futuro. Il Centro per gli studi sulla democrazia di Aarau e il Procivis Think Tank stanno quindi introducendo un indice per la Svizzera che registra la partecipazione politica digitale e mappa i cantoni in modo comparabile. Un tale sistema di monitoraggio non esiste ancora per la Svizzera.$

La partecipazione politica digitale è registrata con l'aiuto di un indice in una gamma di valori standardizzati. I valori per il DigiPartIndex Svizzera vanno da 0 a 100 punti. Le differenze tra i cantoni sono notevoli. Sono stati ottenuti da un minimo di 6 a un massimo di 55 punti. Il valore medio è di 31 punti. La classifica tende ad essere guidata dai cantoni con una maggiore popolazione e con finanze più forti. Tuttavia, anche i cantoni in cima alla classifica, possono ancora migliorare significativamente in tutti i settori.

L'indice misura tre dimensioni della partecipazione politica digitale. La prima dimensione mostra che le decisioni politiche nelle democrazie sono precedute da una fase di formazione dell'opinione. Ricopre strumenti per l'e-deliberazione, l'educazione politica digitale e l'e-trasparenza. La seconda dimensione, ossia la partecipazione, mappa lo scambio istituzionalizzato tra le agenzie governative e la società civile. I due componenti e-consultation ed e-concerns vengono rilevati a questo scopo. In terzo luogo, oltre al dibattito pubblico e allo scambio tra lo Stato e la società, si possono usare strumenti che abilitano digitalmente l'atto di votare ed eleggere. A tal fine, le basi devono essere poste sotto forma di identificazione elettronica, cioè una e-ID, in modo che possa poi essere utilizzata per il voto elettronico così come per la raccolta elettronica, tra le altre cose. Punti bonus o penalità possono essere assegnati per criteri aggiuntivi come l'uso, la cerchia di utenti, la facilità d'uso e la diversità.

L'indice è aggiornato annualmente. Il rapporto completo è disponibile per il download qui sotto, il set di dati è disponibile [**qui**](/it/data).

[{{< picture path="images/thumbnails/DigiPartIndex_2021_it" alt="Rapporto DigiPartIndex 2021" >}}](/docs/DigiPartIndex_2021_it.pdf)
