---
title: "Rapports"
description: "Rapports annuels sur le DigiPartIndex"
draft: false
bg_image: "images/background/unsplash-Oaqk7qqNh_c-edited-2048w"
menu:
  main:
    name: "Rapports"
    identifier: "blog"
    weight: 3
  footer:
    name: "Rapports"
    identifier: "blog"
    weight: 3
---
