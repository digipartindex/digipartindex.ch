---
title: "Das Projekt"
description: "Projektbeschrieb und Team hinter dem DigiPartIndex"
bg_image: "images/background/unsplash-W8KTS-mhFUE-2048w.jpg"
layout: "project"
draft: false
menu:
  main:
    name: "Projekt"
    weight: 2
  footer:
    name: "Projekt"
    weight: 2

########################### project #############################
about:
  enable: true
  title: "DigiPartIndex"
  column_left: "Wir führen für die Schweiz einen Index ein, der digitale politische Partizipation erfasst und Kantone vergleichbar abbildet. Ein solches Monitoring gibt es für die Schweiz noch nicht. Es kann als Ansporn dazu dienen, in der Schweiz im Bereich digitaler Partizipation zu experimentieren.


  Das Konzept «digitale politische Partizipation» soll mit Hilfe eines Index in einem standardisierten Wertebereich erfasst werden. Der Hauptakzent soll auf denjenigen digitalen Instrumenten liegen, die eine effektive politische Partizipation ermöglichen. Wir werden zunächst alle Schweizer Kantone abdecken, wobei aber die Möglichkeit besteht, den Index auf ausgewählte Städte und Gemeinden zu erweitern und auch die Bundesebene miteinzubeziehen.


  Was ist mit digitaler politischer Partizipation im engeren Sinne gemeint? Die UNO hat E-Partizipation – als Synonym für digitale politische Partizipation – definiert als ein Prozess, der Bürgerinnen und Bürger mit Hilfe von Informations- und Kommunikationstechnologien miteinbezieht in Design, Entscheidung und Ausführung von Politik, mit dem Ziel diesen Prozess partizipativ, inklusiv sowie deliberativ auszugestalten.


  In unserem Projekt achten wir darauf, dass ein möglichst breiter Einbezug sämtlicher Bevölkerungsteile Beachtung findet."
  column_right: "E-Partizipation ist nicht exklusiv an die Schweizer Staatsbürgerschaft gekoppelt. Zudem stammen Angebote digitaler politischer Partizipation nicht nur von staatlichen Stellen (top-down), sondern sie können auch von zivilgesellschaftlichen Organisationen angeboten werden (bottom-up).


  Das Konzept «Digitale politische Partizipation» beinhaltet demnach Möglichkeiten, sich während der verschiedenen Phasen des Politikzyklus (Problemdefinition, Agenda Setting, Formulierung und Entscheidung, Implementation, Evaluation) mit Hilfe von Informations- und Kommunikationstechnologien Gehör zu verschaffen sowie Einfluss auf rechtlich bindende Entscheide oder deren Vorbereitung durch staatliche Institutionen zu nehmen. Die entsprechenden digitalen Werkzeuge und Applikationen dazu können, müssen jedoch nicht zwingend von staatlicher Seite zur Verfügung gestellt werden.


  Auf eine einfache Formel gebracht umfasst digitale politische Partizipation ein durch das Internet erleichterter Austausch zwischen Bevölkerung, zivilgesellschaftlichen Organisationen und staatlichen Stellen in den verschiedenen Phasen des politischen Prozesses."

########################## institutions ############################
institutions:
  enable: true
  item:
    - image: "images/logos/mercator_white.svg"
      url: "https://www.stiftung-mercator.ch/"
      color: "primary"
      content: ""

    - image: "images/logos/zda_white.svg"
      url: "https://zdaarau.ch/"
      color: "primary-dark"
      content: ""

    - image: "images/logos/procivis_white.svg"
      url: "https://www.procivis.ch/think-thank/"
      color: "primary-darker"
      content: ""

########################## team Deutschschweiz ############################
team:
  enable: true
  title: "DigiPartIndex-Team"
  intro: "DigiPartIndex wird vom Programm [«Digitalisierung + Gesellschaft» der Stiftung Mercator Schweiz](https://www.stiftung-mercator.ch/stiftung/digitalisierung-gesellschaft) gefördert. Das Projektteam ist als Partnerschaft zwischen dem [Zentrum für Demokratie Aarau (ZDA)](https://zdaarau.ch/) an der Universität Zürich sowie dem [Thinktank von Procivis AG](https://www.procivis.ch/think-thank/) angelegt."
  item:
    - image: "images/people/uwe.jpg"
      name: "Uwe Serdült"
      content: "**Uwe Serdült** (Projektleiter) ist wissenschaftlicher Mitarbeiter am Zentrum für Demokratie Aarau (ZDA) an der Universität Zürich sowie Professor am «e-Society Laboratory» der Ritsumeikan-Universität in Japan. Von 2012–2018 hat er das am ZDA angesiedelte Grossprojekt «E-Democracy» sowie zusammen mit Prof. F. Schweitzer, ETHZ, ein interdisziplinäres SNF-Projekt zum Thema «Emotionalität und Polarisierung in neuen sozialen Medien» geleitet (2013–2016). Seine Aktivitäten in Wissenschaft und Medien im Bereich digitaler Demokratie können am besten seiner [persönlichen Webseite](https://uweserdult.wordpress.com/) entnommen werden."

    - image: "images/people/costa.jpg"
      name: "Costa Vayenas"
      content: "**Costa Vayenas** (Projektleiter) ist Autor des Buches [«Democracy in the Digital Age»](https://books.google.ch/books/about/Democracy_in_the_Digital_Age.html?id=IpCfDwAAQBAJ) und leitet den [Thinktank von Procivis](https://www.procivis.ch/think-thank), der Auswirkungen digitaler Technologien auf die Demokratie untersucht. Neben vielfältiger Vortragstätigkeit im In- und Ausland (u. a. Europäisches Parlament im Bereich E-Government) unterrichtet er momentan auch an der ETH Zürich als Dozent im neuen Nachdiplom-Studiengang [«Technology and Public Policy Programme»](https://tpp.ethz.ch/)."

    - image: "images/people/gabriel.jpg"
      name: "Gabriel Hofmann"
      content: "**Gabriel Hofmann** ist Projektmitarbeiter am Zentrum für Demokratie Aarau&nbsp;(ZDA) der Universität Zürich. Er studiert Politikwissenschaft mit Fokus Schweizer Politik am Institut für Politikwissenschaft Zürich, wo er auch wissenschaftliche Hilfskraft am Lehrstuhl Policy Analyse & Evaluation war. Während seinem Studium hat er sich intensiv mit Bürgerkompetenzen und Entscheidungsprozessen auseinandergesetzt."

    - image: "images/people/marine.jpg"
      name: "Marine Benli-Trichet"
      content: "**Marine Benli-Trichet** ist wissenschaftliche Mitarbeiterin am Zentrum für Demokratie Aarau (ZDA). Seit 2018 ist sie Doktorandin am Institut für Politikwissenschaft der Universität Zürich mit Fokus Digitalisierung und Lokalpolitik. In ihrer Dissertation untersucht sie die Auswirkungen staatsbürgerlicher Technologien auf die Demokratie. Daneben forschte sie zusammen mit dem Laboratorium für Stadtsoziologie (LaSUR) der Eidgenössischen Technischen Hochschule Lausanne (EPFL) im Rahmen des Projekts [«Barometer der \"Civic Tech\" 2021»](https://www.epfl.ch/labs/lasur/fr/das-barometer-der-civic-tech-2021/)."



########################## partners ############################

  partner:
    enable: true
    title: "Partner"
    intro: "Neben dem ZDA und dem Thinktank von Procivis sind noch weitere Institutionen mit dem Projekt assoziiert. Seit dem Sommer 2022 gibt es unter anderem eine Kollaboration mit der [Università della Svizzera Italiana (USI)](https://www.usi.ch/en)."
    item:
      - image: "images/people/jeanpatrick.jpg"
        name: "Jean-Patrick Villeneuve"
        content: "**Jean-Patrick Villeneuve** ist Assistenzprofessor an der Università della Svizzera italiana (USI) in Lugano. Er ist Direktor des Instituts für Kommunikation und Public Policy und der GRIP (Public Integrity Research Group). Er ist Gründungsmitglied der Global Conference on Transparency Research und war bis vor kurzem Mitglied des unabhängigen Experten-Panels der Open Government Partnership.


        Seine Forschung konzentriert sich auf Fragen der Bürgerbeteiligung, Transparenz, Korruptionsbekämpfung und Rechenschaftspflicht. Er interessiert sich besonders für die Grenzen, Schwierigkeiten und Auswirkungen der Umsetzung dieser Governance-Initiativen. Seine Forschung wurde unter anderem vom Schweizerischen Nationalfonds, dem Schweizerischen Netzwerk für Internationale Studien, den Schweizer Universitäten und den Sozialwissenschaften sowie dem Humanities Research Council of Canada finanziert. Prof. Villeneuve hat bei den Vereinten Nationen und in verschiedenen öffentlichen Organisationen gearbeitet."

      - image: "images/people/anna.jpg"
        name: "Anna Picco-Schwendener"
        content: "**Anna Picco-Schwendener**, PhD, ist Postdoktorandin an der Fakultät für Kommunikation, Kultur und Gesellschaft der Università della Svizzera italiana (USI), wo sie E-Government und Online Communication Design lehrt. Sie arbeitet auch als wissenschaftliche Mitarbeiterin im eLearning Lab der USI, wo sie das [Kompetenzzentrum für digitales Recht](http://www.ccdigitallaw.ch) leitet und die USI innerhalb der operativen Einheit des [Lugano Living Lab](https://luganolivinglab.ch) vertritt. Ihre Dissertation befasste sich mit \"Sozialen Dimensionen von grossen Wi-Fi Netzwerken: Die Fälle eines städtischen und eines gemeinschaftlichen drahtlosen Netzwerks\"."

########################## former teammembers ############################

  former_team:
    enable: true
    title: "Ehemalige Teammitglieder"
    item:
      - image: "images/people/herveline.jpg"
        name: "Herveline Du Clary"
        content: "**Herveline Du Clary** hat sich bis Ende 2021 mit ihrem sprach-, kultur- sowie kommunikationswissenschaftlichen Hintergrund um die französische Übersetzung der Webseite und des [Berichtes 2021](/de/blog/digipartindex_2021/) gekümmert."

      - image: "images/people/leonardo.jpg"
        name: "Leonardo Colasante"
        content: "**Leonardo Colasante** hat sich von Mitte 2022 bis Anfang 2023 neben seinem Masterstudium in Public Management and Policy an der Università della Svizzera italiana (USI) in Lugano um die italienische Übersetzung des [Berichtes 2021](/de/blog/digipartindex_2021/) und die Datenerhebung für den Kanton Tessin gekümmert."

############################# contact #################################
contact:
  enable: false
  # content comes from "_index.md"
---
