---
title: "Öffentlichkeit"
description: "Veröffentlichungen, abgehaltene Veranstaltungen und Medienberichterstattung im Zusammenhang mit dem DigiPartIndex"
bg_image: "images/background/unsplash-6M9jjeZjscE-edited-2048w.jpg"
layout: "publicity"
slug: "publicity"
draft: false
menu:
  main:
    name: "Öffentlichkeit"
    weight: 6
  footer:
    name: "Öffentlichkeit"
    weight: 6
---
