---
title: "The Project"
description: "Project description and team behind the DigiPartIndex"
bg_image: "images/background/unsplash-W8KTS-mhFUE-2048w.jpg"
layout: "project"
draft: false
menu:
  main:
    name: "Project"
    weight: 2
  footer:
    name: "Project"
    weight: 2

########################### project #############################
about:
  enable: true
  title: "DigiPartIndex"
  column_left: "We are introducing an index for Switzerland that takes stock of digital political participation and provides a comparable picture of the cantons. Such a monitoring system does not yet exist for Switzerland. It can serve as an incentive to experiment in the area of digital participation in Switzerland.


  The concept of \"digital political participation\" is to be captured with the help of a index in a standardised value range. The main focus will be on those digital tools that enable effective political participation. We will begin by covering all Swiss cantons. However, there is potential to expand the index to selected cities and municipalities, and also to assess the federal level.


  What is meant by digital political participation in the narrower sense? The UN has defined e-participation – as a synonym for digital political participation – as a process that involves citizens in the design, decision-making and implementation of policies through information and communication technologies, with the aim of making the process participatory, inclusive and deliberative.


  In our project, we make sure that the broadest possible inclusion of all segments of the population is taken into account."
  column_right: "E-participation is not exclusively linked to Swiss citizenship. Moreover, offers of digital political participation do not only come from government agencies (top-down), but can also be offered by civil society organisations (bottom-up).


  The concept of \"digital political participation\" thus includes opportunities to make one's voice heard during the various phases of the policy cycle (defining the problem, agenda-setting, formulation and decision-making, implementation, evaluation) with the help of information and communication technologies and to influence legally binding decisions or their preparation by state institutions. The corresponding digital tools and applications can, but do not necessarily have to be, provided by the state.


  In a simple formula, digital political participation involves exchanges facilitated via the internet between the population, civil society organisations and government agencies at different stages of the political process."

########################## institutions ############################
institutions:
  enable: true
  item:
    - image: "images/logos/mercator_white.svg"
      url: "https://www.stiftung-mercator.ch/"
      color: "primary"
      content: ""

    - image: "images/logos/zda_white.svg"
      url: "https://zdaarau.ch/"
      color: "primary-dark"
      content: ""

    - image: "images/logos/procivis_white.svg"
      url: "https://www.procivis.ch/think-thank/"
      color: "primary-darker"
      content: ""

########################## team ############################
team:
  enable: true
  title: "Team and Partner"
  intro: "DigiPartIndex is funded by the programme [\"Digitalisation + Society\" of the Mercator Foundation Switzerland](https://www.stiftung-mercator.ch/stiftung/digitalisierung-gesellschaft). The project team is set up as a partnership between the [Centre for Democracy Studies Aarau (ZDA)](https://zdaarau.ch/) at the University of Zurich and the [Procivis AG think tank](https://www.procivis.ch/think-thank/)."
  item:
    - image: "images/people/uwe.jpg"
      name: "Uwe Serdült"
      content: "**Uwe Serdült** (project leader) is a research associate at the Centre for Democracy Studies Aarau (ZDA) at the University of Zurich and a professor at the \"e-Society Laboratory\" at Ritsumeikan University in Japan. From 2012–2018, he led the large-scale project \"E-Democracy\" based at the ZDA as well as an interdisciplinary SNF project on \"Emotionality and Polarisation in New Social Media\" together with Prof. F. Schweitzer, ETHZ (2013–2016). His activities in science and media in the field of digital democracy can best be taken from his [personal website](https://uweserdult.wordpress.com/)."

    - image: "images/people/costa.jpg"
      name: "Costa Vayenas"
      content: "**Costa Vayenas** (project leader) is the author of the book [\"Democracy in the Digital Age\"](https://books.google.ch/books/about/Democracy_in_the_Digital_Age.html?id=IpCfDwAAQBAJ) and heads the [think tank of Procivis](https://www.procivis.ch/think-thank), which studies the impact of digital technologies on democracy. In addition to a wide range of lecturing activities in Switzerland and invitations to provide policy inputs (including at the European Parliament in the area of e-government), he is currently also teaching at ETH Zurich as a lecturer in the new postgraduate programme [\"Technology and Public Policy Programme\"](https://tpp.ethz.ch/)."

    - image: "images/people/gabriel.jpg"
      name: "Gabriel Hofmann"
      content: "**Gabriel Hofmann** is a research assistant at the Centre for Democracy Studies Aarau (ZDA) at the University of Zurich. He studies political science with a focus on Swiss politics at the Institute of Political Science at the University Zurich, where he was also a research assistant at the Chair of Policy Analysis & Evaluation. During his studies, he intensively studied civic competencies and decision-making processes."

    - image: "images/people/marine.jpg"
      name: "Marine Benli-Trichet"
      content: "**Marine Benli-Trichet** is a research associate at the Center for Democracy Studies Aarau (ZDA). Since 2018, she is a PhD candidate at the Institute of Political Science at the University of Zurich with a focus on digitalization and local politics. In her dissertation, she investigates the impact of civic technologies on democracy. Besides, she conducted research together with the Laboratory of Urban Sociology (LaSUR) of the Swiss Federal Institute of Technology Lausanne (EPFL) within the [\"Barometer of Civic Tech 2021\"](https://www.epfl.ch/labs/lasur/fr/das-barometer-der-civic-tech-2021/) project."


########################## partners ############################

  partner:
    enable: true
    title: "Partners"
    intro: "In addition to the ZDA and the Procivis think tank, other institutions are associated with the project. Since the summer of 2022, there has been a collaboration with the [Università della Svizzera Italiana (USI)](https://www.usi.ch/en)."
    item:
      - image: "images/people/jeanpatrick.jpg"
        name: "Jean-Patrick Villeneuve"
        content: "**Jean-Patrick Villeneuve** is an associate professor at the Università della Svizzera italiana (USI) in Lugano. He is Director of the Institute of Communication and Public Policy and of the GRIP (Public Integrity Research Group). He is founding member of the Global Conference on Transparency Research and, until recently, Member of the Independent Expert Panel of the Open Government Partnership.


        His research focuses on issues of civic participation, transparency, anti-corruption and accountability. He is particularly interested in the limitations, difficulties and impacts of implementing these governance initiatives. His research has been funded by the Swiss National Science Foundation, Swiss Network of International Studies, SwissUniversities and the Social Sciences, the Humanities Research Council of Canada and others. Prof. Villeneuve has worked at the United Nations, and in different public organisations."

      - image: "images/people/anna.jpg"
        name: "Anna Picco-Schwendener"
        content: "**Anna Picco-Schwendener**, PhD, is a postdoctoral researcher at the Faculty of Communication, Culture and Society of the Università della Svizzera italiana (USI) where she teaches eGovernment and Online Communication Design. She also works as scientific collaborator at USI’s eLearning Lab where she is in charge of the [Competence Centre in Digital Law](http://www.ccdigitallaw.ch) and represents USI within the operative unit of the [Lugano Living Lab](https://luganolivinglab.ch). Her PhD dissertation was about \"Social Dimensions of Public Large-Scale Wi-Fi Networks: The Cases of a Municipal and a Community Wireless Network\"."

########################## former teammembers ############################

  former_team:
    enable: true
    title: "Former team members"
    item:
      - image: "images/people/herveline.jpg"
        name: "Herveline Du Clary"
        content: "**Herveline Du Clary**, with her background in language, culture as well as communication studies, took care of the French translation of the website and the [2021 report](/en/blog/digipartindex_2021/) until the end of 2021."
      - image: "images/people/leonardo.jpg"
        name: "Leonardo Colasante"
        content: "**Leonardo Colasante** took care of the Italian translation of the [2021 report](/en/blog/digipartindex_2021/) and the data collection for the canton of Ticino from mid 2022 to early 2023, beside his master's degree in Public Management and Policy at the Università della Svizzera italiana (USI) in Lugano."

############################# contact #################################
contact:
  enable: false
  # content comes from "_index.md"
---
