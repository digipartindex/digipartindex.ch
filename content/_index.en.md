---
menu:
  main:
    name: "Home"
    weight: 1

############################### Banner ##############################
banner:
  enable: true
  bg_image: "images/background/unsplash-B4lknSRZwPM-edited-2048w.jpg"
  bg_overlay: true
  title: "Index of Digital Political Participation in Switzerland"
  content: ""
  button:
    enable: false
    label: ""
    link: ""

############################# About #################################
about:
  enable: true
  title: "The concept of \"digital political participation\""
  description: ""
  content: "...is to be captured with the help of a index in a standardised value range. The main focus will be on those digital tools that enable effective political participation. We will begin by covering all Swiss cantons. However, there is potential to expand the index to selected cities and municipalities, and also to assess the federal level.


    Digital political participation increasingly complements analogue forms of political participation. Elements of the political process such as dialogue, consultation, participation as well as decision-making in the digital space have received a boost again especially in times of COVID-19. Because they correspond to the new digital world and experiences of younger generations, they will continue to play an important role in the future. Digital governance taking place in the context of \"smart cities\" with corresponding tools is spreading worldwide. The situation is particularly dynamic at the sub-national level and in large cities (see, for example, the increasing spread of the open-source participation platform [CONSUL](https://consulproject.org/))."
  image:
    src: "images/logos/digipartindex.svg"
    link: "project"
  button:
    enable: true
    label: "More about the project"
    link: "project"

######################### Data ###############################
data:
  enable: true
  bg_image: "images/background/unsplash-HOrhCnQsxnQ-2048w.jpg"
  title: "Data"
  content: "The data on which the index is based are freely available to the interested public."
  button:
    enable: true
    label: "To the data"
    link: "data"

############################# FAQ ############################
faq:
  enable: false
  title: "A collection of frequently asked questions"
  content: "..."
  button:
    enable: true
    label: "FAQ"
    link: "faq"

############################ Contact ###########################
cta:
  enable: true
  bg_image: "images/background/unsplash-xbrMwBgYUHY-2048w.jpg"
  title: "Open Data, Open Questions?"
  content: "Working with unknown data can initially appear overwhelming. We are happy to help with problems - do not hesitate to..."
  button:
    enable: true
    label: "contact us"
    link: "contact"

############################# Logos ###############################
logos:
  enable: true
  title: "Realised by"
---
