---
title: "Data"
description: "The data and flowcharts behind the DigiPartIndex"
subtitle: ""
bg_image: "images/background/unsplash-trYl7JYATH0-edited-2048w"
layout: "data"
draft: false
menu:
  main:
    name: "Data"
    weight: 5
  footer:
    name: "Data"
    weight: 5

################################ Datasets ################################
datasets:
  enable : true
  items:
  - enable: true
    head : ""
    title: "Dataset"
    content : "_XLSX file_"
    link : "/download/DigiPartIndex.xlsx"
    bullets:
    - "Contains 2 worksheets:"
    - "Main dataset plus metadata"

  - enable: true
    head : ""
    title: "Flowcharts "
    content : "_PDF file_"
    link : "/download/Flow_Charts_Data_Collection.pdf"
    bullets:
    - "Describes the data collection process"

  - enable: false
    name : ""
    price: "CSV"
    content : "_**C**omma-**s**eparated **v**alues text file_"
    link : "#"
    services:
    - Open and flexible
    - Suitable for version control (Git)
    - No column labels<br>(technical limitation)
    - UTF-8 encoded<br>Double quote character (`"`) as string separator

  - enable: false
    name : ""
    price: "ZSAV"
    content : "_Compressed **SPSS** data file_"
    link : "#"
    services:
    - Proprietary
    - Suitable for SPSS users
    - Column names shortened to 32 characters<br>(technical limitation)

  - enable: false
    name : ""
    price: "DTA"
    content : "_**Stata** data file_"
    link : "#"
    services:
    - Proprietary
    - Suitable for Stata users
    - Column names shortened to 32 characters<br>(technical limitation)

############################ Contact ###########################
cta:
  enable: true
  # content comes from "_index.md"
---
