---
title: "Contatta"
description: "Contattateci per domande sul DigiPartIndex"
bg_image: "images/background/unsplash-L5HG3CH_pgc-edited-2048w"
layout: "contact"
draft: false
menu:
  main:
    name: "Contatta"
    weight: 7
  footer:
    name: "Contatta"
    weight: 7
---
