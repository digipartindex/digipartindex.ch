# Static website of the [DigiPartIndex](https://digipartindex.ch/) project

[![Netlify
Status](https://api.netlify.com/api/v1/badges/28cf23ac-e6ab-4b3d-9047-0356fd09ce59/deploy-status)](https://app.netlify.com/sites/digipartindex-ch/deploys)

## Analytics

To measure the number and origin of visitors to the site, we use the non-privacy-invading open source analytics service [Counter](https://counter.dev/). The
service doesn't collect or store any personal information from visitors and there are no cookies involved.

The Counter account is currently managed by @salim-b, but guest access to the analytics dashboard is activated and accessible under [**this
URL**](https://counter.dev/dashboard.html?user=digipartindex&token=KgjTq7pny9TCCLi2).

## E-Mail and contact form

We use a fully managed e-mail solution by [mailbox.org](https://mailbox.org/), for which we configured `digipartindex.ch` as a [custom
domain](https://kb.mailbox.org/en/private/custom-domains) with a [catch-all
alias](https://kb.mailbox.org/en/private/e-mail-article/using-catch-all-alias-with-own-domain). The administration and webmail interface can be accessed
[here](https://login.mailbox.org/) (credentials are managed by @salim-b).

We mainly communicate via the following two e-mail adresses (ending in `digipartindex.ch`):

-   **`info@`** as the main contact address. E-Mails sent to that address are forwarded to @gabrielhofmann.

    Messages submitted via the website's [contact form](https://digipartindex.ch/contact) are delivered to `info@`. The sender of the e-mail is always
    `formresponses@netlify.com`. However, the e-mail's [`Reply-To` header field](https://en.wikipedia.org/wiki/Email#Reply-To) is automatically set to the
    `info@` e-mail address, so that it is possible to simply press *reply* in most e-mail clients and the reply will be sent to the correct address. The subject
    line of the e-mail is freely chosen by the person contacting us.

    The contact form can be prefilled via the URL parameters `name`, `email`, `subject` and `body`. Note that the values need to be
    [percent-encoded](https://en.wikipedia.org/wiki/Percent-encoding), so for example a space is represented as `%20` and the letter `ü` as `%C3%BC`. Example:
    [`https://digipartindex.ch/de/contact/?email=some@one.com&body=Gr%C3%BCezi%20wohl!`](https://digipartindex.ch/de/contact/?email=some@one.com&body=Gr%C3%BCezi%20wohl!)

-   **`dev@`** for all technical issues. E-Mails sent to that address are redirected to @salim-b.

## Redirects

For convenience, [URL redirections](https://developer.mozilla.org/en-US/docs/Web/HTTP/Redirections) have been set up for some of the content so that shorter and
easier-to-remember URLs are available:

| What                              | path                 | redirected to path                |
|-----------------------------------|----------------------|-----------------------------------|
| Blog Post Report `YYYY` (German)  | `/bericht/YYYY`      | `/de/blog/digipartindex_YYYY`     |
| PDF Report `YYYY` (German)        | `/bericht/YYYY/pdf`  | `/docs/DigiPartIndex_YYYY_de.pdf` |
| Blog Post Report `YYYY` (English) | `/report/YYYY`       | `/en/blog/digipartindex_YYYY`     |
| PDF Report `YYYY` (English)       | `/report/YYYY/pdf`   | `/docs/DigiPartIndex_YYYY_en.pdf` |
| Blog Post Report `YYYY` (French)  | `/rapport/YYYY`      | `/fr/blog/digipartindex_YYYY`     |
| PDF Report `YYYY` (French)        | `/rapport/YYYY/pdf`  | `/docs/DigiPartIndex_YYYY_fr.pdf` |
| Blog Post Report `YYYY` (Italian) | `/rapporto/YYYY`     | `/it/blog/digipartindex_YYYY`     |
| PDF Report `YYYY` (Italian)       | `/rapporto/YYYY/pdf` | `/docs/DigiPartIndex_YYYY_it.pdf` |

Note that...

-   `YYYY` is a placeholder for the desired year (4 digits), i.e. `/bericht/2021` will redirected to `/de/blog/digipartindex_2021`.
-   all paths listed above assume the scheme+hostname prefix `https://digipartindex.ch`.
-   more redirections can be set up anytime by adding appropriate [`[[redirects]]`
    entries](https://docs.netlify.com/routing/redirects/#syntax-for-the-netlify-configuration-file) to the [`netlify.toml`](netlify.toml) configuration file.

## Development

### Workflow

Currently, we maintain two permanent branches in this repository:

-   [`master`](https://gitlab.com/zdaarau/websites/digipartindex.ch/-/tree/master): This is the **production** branch which is deployed to
    <https://digipartindex.ch/> by Netlify. It is protected, i.e. only @salim-b has push rights.
-   [`preview`](https://gitlab.com/zdaarau/websites/digipartindex.ch/-/tree/preview): This is @salim-b's preview branch with all preliminary/unfinished content
    enabled. It is deployed to <https://preview--digipartindex-ch.netlify.app/> by Netlify.

For [project members](https://gitlab.com/zdaarau/websites/digipartindex.ch/-/project_members) with write access, the following steps are recommended to
contribute changes:

1.  If not already done, clone this repository:

    ``` sh
    git clone git@gitlab.com:zdaarau/websites/digipartindex.ch.git
    ```

2.  Switch to the `master` branch and pull the latest changes:

    ``` sh
    git switch master
    git pull
    ```

3.  Create a new branch that's based on `master`:

    ``` sh
    git switch --create 'BRANCH-NAME'
    git push --set-upstream origin 'BRANCH-NAME'
    ```

    Note that it's recommended to only use lowercase letters, numbers, underscores (`_`) and minuses (`-`) for branch names.

4.  Make your changes, in as many individual commits as you see fit, and then `git push`.

5.  Finally, open a merge request to incorporate `BRANCH-NAME` into `master`. This can be done by simply visiting the URL
    <https://gitlab.com/zdaarau/websites/digipartindex.ch/-/merge_requests/new?merge_request[source_branch]=BRANCH-NAME> (replace `BRANCH-NAME` with the actual
    branch name).

    @salim-b will then review the changes and, if everything looks ok, merge them, or otherwise request further changes (which can simply be done by pushing
    more commits to branch `BRANCH-NAME`).

    Afterwards, you can delete the local copy of `BRANCH-NAME` via:

    ``` sh
    git branch --delete 'BRANCH-NAME'
    ```

Note that on multiple branches (and consequently merge requests) can be worked simultaneously -- which is actually recommended! Just make sure to base each
branch on the `master` branch as outlined above.

### Useful commands & hints

-   **Preview site locally**: `hugo server --buildDrafts --disableFastRender --ignoreCache`

-   **Update themes** (Git submodules): `git submodule update --remote themes/airspace`

-   To safely **include e-mail addresses in Markdown content**, use the included shortcode `{{< cloak_email "jane.doe@example.com" >}}`!

-   To **create PNG thumbnail images of the PDF files under `static/docs/`**, there's a Bash script included in the repository. Make sure you have
    [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) installed[^readme-1] and [ImageMagick](https://en.wikipedia.org/wiki/ImageMagick) available on your
    [`PATH`](https://en.wikipedia.org/wiki/PATH_(variable)), ideally also [OptiPNG](http://optipng.sourceforge.net/) and
    [pngquant](https://pngquant.org/)[^readme-2].

    To run the script from the root of the repository:

    ``` sh
    cd bash_scripts
    ./create_thumbnails.sh
    ```

-   To **create optimized versions of the raw image files under `raw/images/`**, there's a Bash script included in the repository[^readme-3]. Make sure you have
    [MozJPEG](https://github.com/mozilla/mozjpeg#readme) and [avifenc](https://codelabs.developers.google.com/codelabs/avif#2) available on your
    [`PATH`](https://en.wikipedia.org/wiki/PATH_(variable))[^readme-4].

    To run the script from the root of the repository:

    ``` sh
    cd bash_scripts
    ./optimize_images.sh
    ```

-   To compare Git branches, the following commands might be useful.

    Tell which files of the `gabriels-changes` branch have changed relative to the `master` branch:

    ``` sh
    git diff --name-only master..gabriels-changes
    ```

    Display individual changes in one or more files of the `gabriels-changes` branch relative to the `master` branch:

    ``` sh
    git diff master..gabriels-changes PATH/TO/FILE(S)
    ```

    (Replace `PATH/TO/FILE(S)` with actual file paths relative to the repository root.)

[^readme-1]: Bash should already be installed on recent versions of macOS. On Windows, you can use the [Windows Subsystem for
    Linux](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux).

[^readme-2]: All of these tools are available via Homebrew:

    -   [`imagemagick`](https://formulae.brew.sh/formula/imagemagick#default)
    -   [`optipng`](https://formulae.brew.sh/formula/optipng#default)
    -   [`pngquant`](https://formulae.brew.sh/formula/pngquant#default):

[^readme-3]: Only works on Linux and Windows (via WSL), not macOS, because of the `stat` usage (macOS uses FreeBSD's `stat` [whose API
    differs](https://stackoverflow.com/questions/16391208/print-a-files-last-modified-date-in-bash#comment41250485_16391221)).

[^readme-4]: MozJPEG is availbale from [GitHub](https://github.com/mozilla/mozjpeg/releases/latest) or via [npm](https://www.npmjs.com/package/mozjpeg). avifenc
    currently must be [built from source](https://web.dev/compress-images-avif/#encoding-avif-images-with-avifenc).

### Tables

The *publications*, *presentations and workshops* and *media coverage* tables on the [publicity page](https://digipartindex.ch/en/publicity/) as well as the
[dataset changelog table](https://digipartindex.ch/en/data/#changelog) are generated by the respective [`table_*.html` partials](layouts/partials/) from the
[**`publications.csv`**](static/datasets/publications.csv), [**`presentations.csv`**](static/datasets/presentations.csv),
[**`media_coverage.csv`**](static/datasets/media_coverage.csv) and [**`changelog_data.csv`**](static/datasets/changelog_data.csv) spreadsheets. To modify the
table content, simply edit the respective CSV file, e.g. [via GitLab's built-in Web
IDE](https://gitlab.com/-/ide/project/zdaarau/websites/digipartindex.ch/edit/master/-/static/datasets/media_coverage.csv).

Formatting notes:

-   The `date` columns must conform to [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601). Note that entries lying in the future are not excluded[^readme-5].
-   The `language` columns must conform to [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes).
-   Markdown is supported in the columns `author`, `item_title`, `journal`, `medium`, `place`, `presenter` and `reason`.

[^readme-5]: We used to omit entries with a future `date`, i.e. a site rebuild on or after such entries' `date` was needed to include them. Since this proved to
    cause more inconvenience than benefit, we reverted to simply include all items regardless of their date.

### Cantonal profiles

To add another [canton-year-specific profile page](https://digipartindex.ch/de/cantons/), do:

1.  Add a canton-year-matching row in the data file [`static/datasets/digipartindex.xlsx`](static/datasets/digipartindex.xlsx). Markdown is supported in the
    `justification_*` and `text_*` columns.

2.  Add a Markdown file `content/cantons/{canton_short}/{year}.{lang}.md` where `canton_short` is the uppercase two-letter shortcode (e.g. `AG`), `year` the
    4-digit year number (e.g. `2021`) and `lang` the [ISO 639-1](https://en.wikipedia.org/wiki/List_of_ISO_639-1_codes) lowercase two-letter language identifier
    (e.g. `de`) of the profile.

3.  If the file `content/cantons/{canton_short}/_index.{lang}.md` doesn't already exist, create it (as an empty text file).

4.  Run the R code to (re)generate the website data files and plots (see below).

#### Visualizations

The [interactive cantonal tools score plots](https://digipartindex.ch/de/cantons/ag/2021/#tools-score) are generated via the R script
[`R/gen_plots.R`](R/gen_plots.R). To regenerate all relevant plot files as well as the website data found under [`data/generated`](data/generated), simply run
the following in a system terminal from the root of the repository:

``` sh
cd R
Rscript gen_plots.R
cd ..
```

To edit the plot generation code, make sure to have [RStudio Desktop](https://www.rstudio.com/products/rstudio/download/#download) installed and then open the
project file [`R/r.Rproj`](R/r.Rproj). This ensures, the renv private package library is automatically brought up to date (we use renv's [`"explicit"` snapshot
type](https://rstudio.github.io/renv/reference/snapshot.html#snapshot-type)).

To update the renv package records, run the following after opening the [`R/r.Rproj`](R/r.Rproj) project file in RStudio:

``` r
renv::update(prompt = FALSE)
renv::snapshot(prompt = FALSE)
```

To add a new dependency, add its package name (and `Remotes` source if not on CRAN) to the [`DESCRIPTION`](DESCRIPTION) file and run the following in an R
console:

``` r
renv::install(packages = "PKG_NAME",
              prompt = FALSE)
renv::snapshot(prompt = FALSE)
```

#### Canton emblems

The emblems for the [cantons overview page](https://digipartindex.ch/de/cantons/) were taken from [Wikipedia (Wikimedia
Commons)](https://en.wikipedia.org/wiki/Flags_and_arms_of_cantons_of_Switzerland) and modified with Inkscape. Modifications done:

1.  Removed outer border.
2.  Aligned image to coordinates X0:Y0.
3.  Resized to 420x510px (this aspect ratio was taken from [the official corporate design manual of the canton of
    Luzern](https://www.lu.ch/downloads/lu/sk/Das-Corporate-Design-des-Kantons-Luzern.pdf)).
4.  Adjusted page size to drawing size.

### Miscellaneous

-   Last check for JS/CSS dependency updates: 2021-03-29

-   To update the counter.dev JS script, run (from the root of the repo):

    ``` sh
    curl --silent https://cdn.counter.dev/script.js | minify --type=js | sd '\s*$' '\n' > assets/js/counter.min.js
    ```

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).

----------------------------------------------------------------------------------------------------------------------------------------------------------------
