# TODOs


## Local

- Use the `picture.html` partial for the remaining images (team member pics)

- Try to replace the shadows of PNG thumbnails with CSS -> would make the bash script obsolete, i.e. native Hugo solution

- Define people ("team") in Hugo [data files](https://gohugo.io/templates/data-templates/):
  1. Use language-independent subfolder for stuff that is not language-specific (name, e-mail address etc.)
  2. Use language-dependent subfolder [like @bep suggests](https://discourse.gohugo.io/t/multilingual-data-files/4684/4) for stuff to be translated (CV etc.). See also [this short blog post](https://www.regisphilibert.com/blog/2018/08/hugo-multilingual-part-1-managing-content-translation/#data-files).
  3. Adapt all partials to source content from the above data files.

- "Preloader" abchecken

- Test and activate [`Cross-Origin-*` HTTP Headers](https://www.golem.de/news/http-header-webseiten-vor-spectre-schuetzen-2103-155142-5.html)


## Upstream (Airspace Theme)

- Add [removed OG and Twitter Card meta tags](https://github.com/gohugoio/hugo/pull/8344/files) to `head.html` partial

- Implement [`hreflang`](https://developer.mozilla.org/de/docs/Web/HTML/Element/a#attr-hreflang) attributes for all internal hyperlinks, see [this thread](https://discourse.gohugo.io/t/multilang-code-snippet-for-hreflang-and-x-default/34591) for an example

- Implement map with [Leaflet](https://leafletjs.com/); see [hugo-leaflet](https://github.com/altrdev/hugo-leaflet)

- Add [Markdown Render Hooks](https://gohugo.io/getting-started/configuration-markup/#markdown-render-hooks) for external links and maybe [images](https://moritz.sauer.io/schreibt/2020-06-18-hugo-webp-picture/), too

- Fix colors

- Submit outstanding changes as soon as themefisher releases their [announced theme overhaul](https://github.com/themefisher/airspace-hugo/pull/132#issuecomment-820058879):

  - Commits from the [`rejected/navbar-and-scss-vars` branch](https://github.com/salim-b/airspace-hugo/tree/rejected/navbar-and-scss-vars):
    - [Outsource style for responsive page height into separate class](https://github.com/salim-b/airspace-hugo/commit/3a341d5a55da63f9349d967a0e81c8fe43e74497)
    - [Allow setting SCSS variables in `config.toml`](https://github.com/salim-b/airspace-hugo/commit/d90d83e3271be01d171cba7cbeb3f48fe5820aca)
    - [Improve / simplify navigation bar styling](https://github.com/salim-b/airspace-hugo/commit/9870bfa91e4d2d12b155f3b367c44e578e36dcc6)

- As soon as Hugo's [Dart Sass](https://gohugo.io/hugo-pipes/scss-sass/#options) integration is out of beta, migrate the theme's `.scss` files [to the newer `@use` rule](https://sass-lang.com/documentation/cli/migrator)

- Selfhost fonts; currently external fonts defined in `themes/airspace/assets/scss/_typography.scss` are used; the fonts were already once served locally, see [this commit](https://github.com/themefisher/airspace-hugo/commit/e6e6b031d11faa8ad62d812c93beb473db997b38) from 2018-05-21

- Add taxonomy term translation, e.g. [like this](https://discourse.gohugo.io/t/internationalize-taxonomy-terms/27187/6); see also [here](https://stackoverflow.com/a/57359213/7196903); plus [must-read](https://gohugo.io/templates/taxonomy-templates/)

- Figure out why source maps produced by `... | toCSS (dict "enableSourceMap" true)` are not recognized by browsers...

- Update [Ionicons](https://ionicons.com/), ideally as a PR for the  [Airspace theme](https://github.com/themefisher/airspace-hugo): Currently Ionicons v2 are used while [v5 is the latest](https://github.com/ionic-team/ionicons/releases)

  - From v5 onwards, Ionicons have to be included/loaded as JS (module), seems not possible through CSS anymore. To load them, the [following](https://github.com/ionic-team/ionicons/issues/821#issuecomment-720108400) has to be added to your `<head>`:

    ```html
    <script src="/ionicons/ionicons.esm.js" type="module"></script>
    <script nomodule src="/ionicons/ionicons.js"></script>
    ```
    (the 2nd `<script>` tag with `nomodule` is for older browsers withoug JS module support; the file was [removed/moved in v5.3.0](https://github.com/ionic-team/ionicons/issues/821#issuecomment-808941130), so the above has to be updated...)

    The whole Ionicons files distro can be installed [from npm](https://www.npmjs.com/package/ionicons). This npm package could be added as a [Hugo module](https://gohugo.io/hugo-modules/) to Hugo's `config.toml` as [follows](https://discourse.gohugo.io/t/importing-js-from-node-packages/23545/4):

    ```toml
    [module]
      [[module.mounts]]
        source = "node_modules/ionicons/dist/ionicons"
        target = "static/ionicons"
    ```

  - Update icon references since some icon names have [changed in v5](https://github.com/ionic-team/ionicons/releases/tag/5.0.0).

    To search for affected icons in the theme, run in a shell:

    ```sh
    ack '(icon|class) ?[:=] ?"ion-.*\b(add-circle|alert|appstore|arrow-dropdown-circle|arrow-dropdown|arrow-dropleft-circle|arrow-dropleft|arrow-dropright-circle|arrow-dropright|arrow-dropup-circle|arrow-dropup|arrow-round-back|arrow-round-down|arrow-round-forward|arrow-round-up|at|bowtie|chatboxes|clock|contact|contacts|done-all|fastforward|filing|logo-freebsd-devil|logo-game-controller-a|logo-game-controller-b|logo-googleplus|hand|heart-empty|jet|list-box|lock|microphone|model-s|more|notifications-outline|outlet|paper|logo-polymer|pie|photos|qr-scanner|quote|redo|reorder|return-left|return-right|rewind|reverse-camera|share-alt|skip-backward|skip-forward|stats|swap|switch|text|undo|unlock)\b'
    ```

    Affected are the following icon names:

    - `chatboxes` -> `chatbox`
    - `quote` -> removed; an alternative could be to simply rely on the Unicode [LEFT DOUBLE QUOTATION MARK `“`](https://www.cl.cam.ac.uk/~mgk25/ucs/quotes.html)

  - Or maybe better directly switch to [Bootstrap Icons](https://icons.getbootstrap.com/)? They seem more promising (already 1'300+ icons, SVG-first (no JS bullshit) and not tied to Bootstrap as Ionicons are tied to Ionic Framework)...
