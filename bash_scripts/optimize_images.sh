#!/bin/bash

# NOTES:
# - Only works on Linux, not macOS because of the `stat` usage (macOS uses FreeBSD's stat [whose API differs](https://stackoverflow.com/questions/16391208/print-a-files-last-modified-date-in-bash#comment41250485_16391221))

# ensure avifenc is available
# to compile from source, see: https://codelabs.developers.google.com/codelabs/avif#2
if ! command -v avifenc >/dev/null ; then
  echo "ERROR: libavif's \`avifenc\` not found."
  exit 1
fi

# ensure mozjpeg is available
# get it from [GitHub](https://github.com/mozilla/mozjpeg/releases/latest) or via [npm](https://www.npmjs.com/package/mozjpeg)
if ! command -v mozjpeg >/dev/null ; then
  echo "ERROR: Mozilla JPEG Encoder \`mozjpeg\` not found."
  exit 1
fi

image_dest_dir=`dirname "$PWD"`/static/images

for dir in ../raw/images/* ; do

  # abort in case of empty images dir
  [ -d "$dir" ] || continue

  if [[ `basename "$dir"` != "UNUSED" ]] ; then

    dir_basename=`basename "$dir"`

    for file in "$dir"/*.jpg ; do

      # abort in case of empty dir
      [ -f "$file" ] || continue

      file_basename=`basename -s ".jpg" "$file"`
      output_jpg="${image_dest_dir}/${dir_basename}/${file_basename}.jpg"
      output_avif="${image_dest_dir}/${dir_basename}/${file_basename}.avif"
      # create optimized JPEG image
      if [ ! -f "$output_jpg" ] || [ `stat -c %Y $file` -gt `stat -c %Y $output_jpg` ] ; then
        mozjpeg -dc-scan-opt 2 -quality 85 "$file" > "$output_jpg"
      fi
      # create AVIF image (using settings recommended by Google, cf. https://codelabs.developers.google.com/codelabs/avif#3)
      if [ ! -f "$output_avif" ] || [ `stat -c %Y $file` -gt `stat -c %Y $output_avif` ] ; then
        avifenc --speed 0 \
                --min 0 \
                --max 63 \
                -a end-usage=q \
                -a cq-level=32 \
                -a tune=ssim \
                -a deltaq-mode=3 \
                -a sharpness=3 \
                -y 420 \
                --output "$output_avif" \
                "$file"
      fi
    done
  fi
done
